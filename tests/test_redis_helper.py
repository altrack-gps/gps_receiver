from asynctest import TestCase
from aiohttp.test_utils import unittest_run_loop

from serializers import TruckStateMsg
from redis.helpers import save_truck_message
from redis.connection import Redis
from tests.utils import get_test_truck_message


class TestRedisHelpers(TestCase):
    def setUp(self):
        super().setUp()
        redis = self.loop.run_until_complete(
            Redis.create('redis://localhost:6379')
        )
        Redis.set_current(redis)

    def tearDown(self):
        redis = Redis.get_current()
        self.loop.run_until_complete(
            redis.close()
        )
        super().tearDown()

    @unittest_run_loop
    async def test_saving_state(self):
        message = get_test_truck_message()
        await save_truck_message(message)

        redis = Redis.get_current().conn
        loaded_message = await redis.hgetall(f'truck_{message.truck_id}')
        loaded_message = {
            k.decode(): v.decode()
            for k, v in loaded_message.items()
        }

        self.assertDictEqual(
            message.dict(),
            TruckStateMsg(**loaded_message).dict()
        )
        await redis.delete(f'truck_{message.truck_id}')
