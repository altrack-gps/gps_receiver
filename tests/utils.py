from random import randint
from datetime import datetime

from db.models import Truck
from serializers import GPSMessage, TruckStateMsg
from gps_producer.alt_protocol import ALTProto


def assert_arg_in_mock_args(arg, mock):
    mock_args = tuple(mock.await_args)[0]
    assert arg in mock_args


def get_test_truck() -> Truck:
    return Truck(
        id=1,
        imei='11',
        plate_number='АА1111АА'
    )


def get_test_truck_message() -> TruckStateMsg:
    return TruckStateMsg(
        truck_id=1,
        **get_test_gps_message().dict()
    )


def get_test_gps_message():
    return GPSMessage(
        imei='111'.zfill(16),
        speed=randint(1, 150),
        course=randint(1, 360),
        state_at=datetime.now(),
        lat=randint(0, 90),
        lon=randint(0, 180)
    )


def get_test_encoded_message():
    return ALTProto().encode_message(
        get_test_gps_message()
    )
