from aioredis import Redis as RedisPool, create_redis_pool

from mixins import ContextInstanceMixin


class Redis(ContextInstanceMixin):
    _redis: RedisPool

    @classmethod
    async def create(cls, connection_str: str):
        self = cls()
        self._redis = await create_redis_pool(connection_str)

        return self

    async def close(self):
        self._redis.close()
        await self._redis.wait_closed()

    @property
    def conn(self):
        return self._redis
