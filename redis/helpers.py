from typing import NoReturn
from datetime import datetime

from pydantic import ValidationError

from utils.dt import from_dt_to_str
from exceptions import GPSDoesNotExist, RoutePresenceLogDoesNotExist
from serializers import GPSMessage, RoutePresenceLog
from redis.connection import Redis


async def get_hm_dict(redis, key) -> dict:
    data_b = await redis.hgetall(key)
    return {
        k.decode(): v.decode()
        for k, v in data_b.items()
    }


async def save_gps_message(truck_id: int,
                           msg: GPSMessage) -> NoReturn:
    data = msg.json_dict()
    redis = Redis.get_current().conn

    await redis.hmset_dict(f'truck_{truck_id}', data)
    await redis.publish_json(f'truck_{truck_id}', data)


async def get_gps_message(truck_id: int) -> GPSMessage:
    redis = Redis.get_current().conn

    try:
        data = await get_hm_dict(redis, f'truck_{truck_id}')
        return GPSMessage(**data)
    except ValidationError:
        raise GPSDoesNotExist


TRUCK_EVENT_CHANNEL = 'truck_events'


async def push_truck_event_msg(e):
    redis = Redis.get_current().conn
    await redis.publish(TRUCK_EVENT_CHANNEL, e.json())


async def save_wp_passing(truck_id: int, wp_id: int):
    redis = Redis.get_current().conn
    data = {
        'wp_id': wp_id,
        'passed_at': from_dt_to_str(datetime.utcnow())
    }

    await redis.hmset_dict(f'truck_{truck_id}_last_wp', data)


async def get_last_wp_passing(truck_id: int):
    redis = Redis.get_current().conn
    return await get_hm_dict(redis, f'truck_{truck_id}_last_wp')


async def save_route_log(log: RoutePresenceLog) -> NoReturn:
    redis = Redis.get_current().conn
    await redis.hmset_dict(
        f"truck_route_log_{log.truck_id}",
        log.dict()
    )


async def get_route_log(truck_id: int) -> RoutePresenceLog:
    redis = Redis.get_current().conn
    log = await get_hm_dict(redis, f'truck_route_log_{truck_id}')

    if not log:
        raise RoutePresenceLogDoesNotExist

    return RoutePresenceLog(**log)
