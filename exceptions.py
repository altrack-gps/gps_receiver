class TruckWithoutRoute(Exception):
    ...


class TruckDoesNotExist(Exception):
    ...


class GPSDoesNotExist(Exception):
    ...


class EventIsNotTriggered(Exception):
    ...


class RoutePresenceLogDoesNotExist(Exception):
    ...
