import asyncio
import logging

from utils.config import read_config
from gps_producer.server import GPSServer
from gps_producer.settings import CONFIG_FILE


def main():
    init_logging()
    config = read_config(CONFIG_FILE)
    asyncio.run(start_server(config))


async def start_server(config):
    server = await GPSServer.create(config)
    try:
        await server.listen()
    finally:
        await server.shutdown()


def init_logging(file_name='gps_producer.log'):
    file = logging.FileHandler(filename=file_name)

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)

    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s|gps_producer|%(filename)20s[%(lineno)3s] %(levelname)8s : %(message)s',
        handlers=[console, file]
    )


if __name__ == '__main__':
    main()
