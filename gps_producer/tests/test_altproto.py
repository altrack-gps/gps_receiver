from unittest import TestCase

from tests.utils import get_test_gps_message
from gps_producer.alt_protocol import ALTProto


class TestAltProto(TestCase):
    def setUp(self):
        super().setUp()
        self.proto = ALTProto()

    def test_proto(self):
        message = get_test_gps_message()

        encoded_message = self.proto.encode_message(message)
        decoded_message = self.proto.decode_message(encoded_message)

        self.assertDictEqual(message.dict(), decoded_message.dict())
