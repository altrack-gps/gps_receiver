import asyncio

from asynctest import TestCase, patch

from utils.config import read_config
from tests.utils import get_test_encoded_message, get_test_truck

from gps_producer.server import GPSServer
from gps_producer.settings import CONFIG_FILE
from gps_producer.alt_protocol import ALTProto


class GPSServerTestCase(TestCase):
    TEST_PORT = 8089

    def setUp(self):
        super().setUp()

        self.server_task = None
        self.config = read_config(CONFIG_FILE)
        self.config['tcp']['port'] = str(self.TEST_PORT)
        self.client = TestGPSClient(**self.config['tcp'])

    async def run_server(self):
        self.server = await GPSServer.create(self.config)
        self.server_task = asyncio.create_task(self.server.listen())
        await asyncio.sleep(1)

    async def stop_server(self):
        self.server_task.cancel()
        await self.server.shutdown()


class TestGPSServer(GPSServerTestCase):
    @patch('gps_producer.server.GPSServer._handle_message')
    async def test_message_handling(self, handle_mock):
        await self.run_server()

        message = get_test_encoded_message()
        await self.client.send_test_message(message)
        await self.stop_server()

        handle_mock.assert_awaited_with(message)

    @patch('gps_producer.server.get_truck_by_imei')
    @patch('gps_producer.server.TruckService.save_state')
    @patch('gps_producer.server.TruckMsgService.save')
    async def test_receiving_gps_state(
            self, truck_message_mock,
            truck_state_mock,
            get_truck_mock
    ):
        truck = get_test_truck()
        get_truck_mock.return_value = truck

        await self.run_server()

        message = get_test_encoded_message()
        await self.client.send_test_message(message)
        await self.stop_server()

        decoded_message = ALTProto().decode_message(message)
        truck_state_mock.assert_awaited_with(truck.id, decoded_message)
        truck_message_mock.assert_awaited_with(truck, decoded_message)


class LoadTest(GPSServerTestCase):
    async def test_many_clients(self):
        await self.run_server()
        client = TestGPSClient(**self.config['tcp'])
        message = get_test_encoded_message()
        tasks = [
            client.send_test_message(message)
            for _ in range(1500)
        ]
        await asyncio.gather(*tasks)

        await asyncio.sleep(5)
        await self.stop_server()


class TestGPSClient:
    def __init__(self, host, port):
        self._host = host
        self._port = port

    async def send_test_message(self, message: bytes):
        reader, writer = await asyncio.open_connection(self._host, self._port)
        writer.write(message)

        writer.close()
        await writer.wait_closed()
