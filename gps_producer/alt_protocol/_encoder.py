import struct
from datetime import datetime

from serializers import GPSMessage


class _ALTProtoEncoder:
    @classmethod
    def encode_message(cls, message: GPSMessage) -> bytes:
        state_at = cls._encode_datetime(message.state_at)
        speed = cls._encode_int('>H', message.speed)
        course = cls._encode_int('>H', message.course)

        encoded_lat = cls._encode_coordinate(message.lat)
        encoded_lon = cls._encode_coordinate(message.lon)

        encoded_state = (
            state_at + speed + course + encoded_lat + encoded_lon + message.imei
        ).encode()

        return encoded_state

    @classmethod
    def _encode_int(cls,
                    fmt: str,
                    value: int) -> str:
        return struct.pack(fmt, value).hex()

    @classmethod
    def _encode_datetime(cls, dt: datetime) -> str:
        current_dt = int(dt.timestamp() * 1000000)
        encoded = struct.pack('>Q', current_dt).hex()
        return encoded

    @classmethod
    def _encode_coordinate(cls, coordinate) -> str:
        encoded = abs(int(coordinate * 1000000))
        return struct.pack('>I', encoded).hex()
