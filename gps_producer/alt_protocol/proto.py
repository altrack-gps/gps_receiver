from serializers import GPSMessage

from ._encoder import _ALTProtoEncoder
from ._decoder import _ALTProtoDecoder


class ALTProto:
    def __init__(self):
        self._encoder = _ALTProtoEncoder()
        self._decoder = _ALTProtoDecoder()

    def encode_message(self, message: GPSMessage) -> bytes:
        return self._encoder.encode_message(message)

    def decode_message(self, message: bytes) -> GPSMessage:
        return self._decoder.decode_message(message)
