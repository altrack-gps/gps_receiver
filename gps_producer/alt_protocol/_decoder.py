import re
import struct

from datetime import datetime

from serializers import GPSMessage


class _ALTProtoDecoder:
    @classmethod
    def decode_message(cls, gps_state: bytes) -> GPSMessage:
        state_at, speed, course, lat, lon, imei = cls._parse_gps_state(gps_state)
        message = GPSMessage(
            imei=imei,
            lat=cls._decode_coord(lat),
            lon=cls._decode_coord(lon),
            speed=cls._decode_int('>H', speed),
            course=cls._decode_int('>H', course),
            state_at=cls._decode_timestamp(state_at),
        )

        return message

    @classmethod
    def _parse_gps_state(cls, gps_state: bytes) -> tuple:
        decoded_state = gps_state.decode()
        state_re = ''.join([
            f'([0-9a-fA-F]{{{size}}})'
            for size in GPSStateFields.values()
        ])
        return re.findall(state_re, decoded_state)[0]

    @classmethod
    def _decode_timestamp(cls, encoded: str) -> datetime:
        timestamp = cls._decode_int('>Q', encoded)
        return datetime.fromtimestamp(timestamp / 1000000)

    @classmethod
    def _decode_int(cls,
                    fmt: str,
                    value: str) -> int:
        return int(struct.unpack(fmt, bytes.fromhex(value))[0])

    @classmethod
    def _decode_coord(cls, value: str) -> float:
        coord = struct.unpack('>I', bytes.fromhex(value))[0]
        coord /= 1000000
        return coord


GPSStateFields = {
    'state_at': 16,
    'speed': 4,
    'course': 4,
    'lat': 8,
    'lon': 8,
    'imei': 16
}
