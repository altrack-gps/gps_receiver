from db.models import Truck, TruckEvent
from exceptions import EventIsNotTriggered
from serializers import GPSMessage, TruckEventType

from gps_producer.events.detector import EventDetector
from gps_producer.events.handlers import EventHandler, OnStartRoute, OnFinishRoute


class TruckEventManager:
    def __init__(self,
                 truck: Truck,
                 gps_message: GPSMessage):
        self._truck = truck
        self._msg = gps_message

    async def handle_events(self):
        detector = EventDetector(self._truck, self._msg)
        try:
            event = await detector.get_event()
            handler = self._get_event_handler(event)
            await handler.handle()
        except EventIsNotTriggered:
            ...

    def _get_event_handler(self, e: TruckEvent) -> EventHandler:
        params = {
            'truck': self._truck,
            'event': e
        }
        handlers = {
            TruckEventType.START_ROUTE: OnStartRoute,
            TruckEventType.FINISH_ROUTE: OnFinishRoute,
        }
        try:
            handler = handlers[e.type]
        except KeyError:
            handler = EventHandler

        return handler(**params)
