from datetime import datetime

from db.models import TruckEvent, Truck
from db.helpers import get_closest_wp, get_address_by_wp_id, get_last_wp, current_report_exists
from exceptions import RoutePresenceLogDoesNotExist, EventIsNotTriggered

from utils.dt import from_str_to_dt, dt_delta
from utils.geom import to_point_from_coords

from serializers import TruckEventType, GPSMessage
from redis.helpers import get_last_wp_passing, save_wp_passing, get_route_log


class EventDetector:
    def __init__(self,
                 truck: Truck,
                 msg: GPSMessage):
        self._truck = truck
        self._msg = msg

    async def get_event(self) -> TruckEvent:
        passing_e = await self._get_passing_e()
        presence_e = await self._get_presence_e()

        if not passing_e and not presence_e:
            raise EventIsNotTriggered

        return presence_e or passing_e

    async def _get_passing_e(self):
        point = to_point_from_coords(self._msg.lat, self._msg.lon)
        wp = await get_closest_wp(self._truck.id, point)
        report_exists = await current_report_exists(self._truck.id)

        if wp:
            address = await get_address_by_wp_id(wp.id)
            last_wp_passing = await get_last_wp_passing(self._truck.id)

            is_updated = _is_passing_updated(wp, last_wp_passing)

            if is_updated:
                await save_wp_passing(self._truck.id, wp.id)
                last_wp = await get_last_wp(wp.route_id)
                e_type = _get_type_by_wp_num(wp.number, last_wp.number)

                if e_type is TruckEventType.START_ROUTE or report_exists:
                    return self._create_e(
                        type=e_type,
                        payload={
                            'address': address.to_str()
                        }
                    )

    def _create_e(self, **kwargs):
        return TruckEvent(
            truck_id=self._truck.id,
            **kwargs
        )

    async def _get_presence_e(self) -> TruckEvent:
        try:
            presence_log = await get_route_log(self._truck.id)
            if presence_log.has_route:
                was_on_route = presence_log.was_on_route
                is_on_route = presence_log.is_on_route

                if was_on_route and not is_on_route:
                    return self._create_e(type=TruckEventType.LEFT_ROUTE)

                elif not was_on_route and is_on_route:
                    return self._create_e(type=TruckEventType.MOVE_ON_ROUTE)
        except RoutePresenceLogDoesNotExist:
            ...


MIN_INTERVAL = 5


def _is_passing_updated(wp, last_passing) -> bool:
    if last_passing:
        passing_dt = from_str_to_dt(last_passing['passed_at'])
        diff = dt_delta(datetime.utcnow(), passing_dt)

        long_ago = diff >= MIN_INTERVAL

        if wp.id == int(last_passing['wp_id']) and not long_ago:
            return False
    return True


def _get_type_by_wp_num(wp_num, max_num) -> TruckEventType:
    types = {
        0: TruckEventType.START_ROUTE,
        max_num: TruckEventType.FINISH_ROUTE
    }
    try:
        return types[wp_num]
    except KeyError:
        return TruckEventType.PASSING_WAY_POINT
