from typing import NoReturn

from db.models import Truck, TruckEvent
from db.helpers import save_truck_event, create_report, finish_report
from serializers import TruckEventMsg
from redis.helpers import push_truck_event_msg
from gps_producer.services import TruckService


class EventHandler:
    def __init__(self,
                 truck: Truck,
                 event: TruckEvent):
        self._truck = truck
        self._event = event

    async def handle(self) -> NoReturn:
        await self._save()

    async def _save(self) -> NoReturn:
        await save_truck_event(self._event)
        e_msg = TruckEventMsg.from_truck_event(self._truck, self._event)
        await push_truck_event_msg(e_msg)


class OnStartRoute(EventHandler):
    async def handle(self) -> NoReturn:
        await super().handle()
        await create_report(
            truck_id=self._truck.id,
            route_id=self._truck.route_id
        )


class OnFinishRoute(EventHandler):
    async def handle(self) -> NoReturn:
        await super().handle()
        await finish_report(self._truck.id)
        await TruckService.take_off_from_route(self._truck.id)
