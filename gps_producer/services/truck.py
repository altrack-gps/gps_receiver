from db.models import TruckState
from db.helpers import get_route_by_id, get_distance_from_route, save_truck_state, update_truck_route

from utils.geom import to_point_from_coords
from serializers import GPSMessage


MAX_DISTANCE_FROM_ROUTE = 1


class TruckService:
    @classmethod
    async def is_on_route(cls,
                          route_id: int,
                          lat: float,
                          lon: float) -> bool:
        route = await get_route_by_id(route_id)
        point = to_point_from_coords(lat, lon)
        distance = await get_distance_from_route(route.line, point)

        return distance < MAX_DISTANCE_FROM_ROUTE

    @classmethod
    async def save_state(cls,
                         truck_id: int,
                         message: GPSMessage) -> TruckState:
        truck_state = TruckState.from_message(truck_id, message)
        await save_truck_state(truck_state)
        return truck_state

    @classmethod
    async def take_off_from_route(cls, truck_id: int):
        await update_truck_route(truck_id, None)
