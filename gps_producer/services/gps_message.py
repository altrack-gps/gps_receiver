from typing import NoReturn

from serializers import GPSMessage
from redis.helpers import get_gps_message, save_gps_message


class GPSMsgService:
    @classmethod
    async def get_last(cls, truck_id: int):
        return await get_gps_message(truck_id)

    @classmethod
    async def save(cls,
                   truck_id: int,
                   message: GPSMessage) -> NoReturn:
        await save_gps_message(
            truck_id,
            message
        )
