from typing import NoReturn

from db.models import Truck
from db.helpers import has_truck_route
from exceptions import RoutePresenceLogDoesNotExist
from serializers import GPSMessage, RoutePresenceLog
from redis.helpers import save_route_log, get_route_log
from gps_producer.services import TruckService


async def update_route_log(truck: Truck,
                           gps_message: GPSMessage):
    await RoutePresenceLogService(
        truck=truck,
        gps_message=gps_message
    ).create()


class RoutePresenceLogService:
    def __init__(self,
                 truck: Truck,
                 gps_message: GPSMessage):
        self._truck = truck
        self._msg = gps_message
        self._is_on_route = False
        self._has_route = False

    async def create(self) -> NoReturn:
        new_log = await self._create_log()
        await save_route_log(new_log)

    async def _create_log(self) -> RoutePresenceLog:
        await self._determine_presence()
        last_log = await self._get_last_log()

        return RoutePresenceLog(**{
            'truck_id': self._truck.id,
            'is_on_route': self._is_on_route,
            'has_route': self._has_route,
            'was_on_route': last_log.is_on_route,
            'had_route': last_log.has_route
        })

    async def _determine_presence(self) -> NoReturn:
        self._has_route = int(await has_truck_route(self._truck.imei))

        if self._has_route:
            self._is_on_route = int(
                await TruckService.is_on_route(
                    route_id=self._truck.route_id,
                    lat=self._msg.lat,
                    lon=self._msg.lon
                )
            )

    async def _get_last_log(self) -> RoutePresenceLog:
        try:
            log = await get_route_log(self._truck.id)
        except RoutePresenceLogDoesNotExist:
            log = RoutePresenceLog(truck_id=self._truck.id)
        return log
