import asyncio
import logging

from gps_producer.alt_protocol import ALTProto
from gps_producer.events.manager import TruckEventManager

from gps_producer.services import TruckService
from gps_producer.services import GPSMsgService
from gps_producer.services.route_log import update_route_log

from db.helpers import get_truck_by_imei
from utils.db.engine import DBEngine
from redis.connection import Redis


class GPSServer:
    def __init__(self):
        self._config = None
        self._server = None

    @classmethod
    async def create(cls, config) -> 'GPSServer':
        self = cls()
        self._config = config['tcp']

        engine = await DBEngine.create(config['postgres']['dsn'])
        DBEngine.set_current(engine)

        redis = await Redis.create(config['redis']['address'])
        Redis.set_current(redis)

        return self

    async def listen(self):
        self._server = await asyncio.start_server(
            client_connected_cb=self._receive_message,
            host=self._config['host'],
            port=self._config.getint('port', 8090)
        )
        async with self._server:
            logging.info(f"====== Running on {self._config['host']}:{self._config['port']} ======")
            await self._server.serve_forever()

    async def _receive_message(
            self,
            reader: asyncio.StreamReader,
            writer: asyncio.StreamWriter
    ):
        client_address = writer.get_extra_info('peername')
        logging.info(f'---connection from {client_address}---')

        while True:
            data = await reader.read(100)
            if not data:
                break
            await self._handle_msg(data)
            logging.info(f'received data {data} from {client_address}')

        await writer.drain()

        writer.close()
        logging.info(f'---connection closed {client_address}---')

    async def _handle_msg(self, message: bytes):
        gps_message = ALTProto().decode_message(message)
        truck = await get_truck_by_imei(gps_message.imei)

        await TruckService.save_state(truck.id, gps_message)
        await GPSMsgService.save(truck.id, gps_message)
        await update_route_log(truck, gps_message)

        event_manager = TruckEventManager(truck, gps_message)
        await event_manager.handle_events()

    async def shutdown(self):
        await Redis.get_current().close()
        await DBEngine.get_current().close()

        if self._server.is_serving():
            self._server.close()
            await self._server.wait_closed()
