import asyncio
import logging

from abc import ABC, abstractmethod

from aiohttp import web
from aiojobs._job import Job
from aiojobs.aiohttp import spawn

from gps_consumer.subscriptions.channel_producer import ChannelProducer


class Subscription(ABC):
    def __init__(
            self,
            ch_name: str,
            request: web.Request,
            ws: web.WebSocketResponse
    ):
        self._ch_name = ch_name
        self._request = request
        self._ws = ws

    @property
    def _app(self):
        return self._request.app

    async def start(self) -> Job:
        await self._on_startup()
        await self._init_producer()
        return await self._run()

    async def _on_startup(self):
        ...

    @abstractmethod
    async def _init_producer(self):
        ...

    async def _run(self) -> Job:
        truck_job = await _run_subscription(
            request=self._request,
            ws=self._ws,
            producer=self._get_producer()
        )
        return truck_job

    @abstractmethod
    def _get_producer(self) -> ChannelProducer:
        ...


async def _run_subscription(
        request: web.Request,
        ws: web.WebSocketResponse,
        producer: ChannelProducer
):
    """
    :return: aiojobs._job.Job object
    """
    listen_redis_job = await spawn(
        request,
        _read_subscription(
            ws,
            producer
        )
    )
    return listen_redis_job


async def _read_subscription(
        ws: web.WebSocketResponse,
        producer: ChannelProducer
):
    try:
        async for msg in producer:
            await ws.send_json(msg)
    except asyncio.CancelledError:
        pass
    except Exception as e:
        logging.error(msg=e, exc_info=e)
