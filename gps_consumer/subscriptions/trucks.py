from gps_consumer.subscriptions._base import Subscription
from gps_consumer.subscriptions.channel_producer import ChannelProducer
from redis.helpers import get_hm_dict


class TruckSubscription(Subscription):
    async def _on_startup(self):
        await self._send_last_data()

    async def _send_last_data(self):
        data = await get_hm_dict(
            self._app['redis'],
            self._ch_name
        )
        await self._ws.send_json(data)

    async def _init_producer(self):
        if self._ch_name not in self._app['truck_state_producers']:
            channel, = await self._app['redis'].subscribe(self._ch_name)
            self._app['truck_state_producers'][self._ch_name] = ChannelProducer(channel)

    def _get_producer(self):
        return self._request.app['truck_state_producers'][self._ch_name]
