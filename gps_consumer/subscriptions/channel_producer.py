import asyncio

from aioredis import Channel


class ChannelProducer:
    def __init__(self, channel: Channel):
        self._channel = channel
        self._future: asyncio.Future = None

    def close(self):
        self._channel.close()

    def __aiter__(self):
        return self

    def __anext__(self):
        return asyncio.shield(self._get_message())

    async def _get_message(self):
        if self._future:
            return await self._future

        self._future = asyncio.get_event_loop().create_future()
        message = await self._channel.get_json()
        future, self._future = self._future, None
        future.set_result(message)

        return message
