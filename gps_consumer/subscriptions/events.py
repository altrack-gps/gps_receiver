from gps_consumer.subscriptions._base import Subscription
from gps_consumer.subscriptions.channel_producer import ChannelProducer


class EventSubscription(Subscription):
    async def _init_producer(self):
        if not self._app['truck_events_producer']:
            channel, = await self._app['redis'].subscribe(self._ch_name)
            self._app['truck_events_producer'] = ChannelProducer(channel)

    def _get_producer(self):
        return self._request.app['truck_events_producer']
