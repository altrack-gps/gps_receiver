import logging
import aiohttp

from aiohttp import web

from gps_consumer.subscriptions import EventSubscription
from gps_consumer.subscriptions import TruckSubscription


truck_routes = web.RouteTableDef()


@truck_routes.get('/truck-gps-state/{id}', name='truck_gps_state')
async def truck_state_view(request: web.Request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    try:
        truck_id = int(request.match_info['id'])
        channel_name = f'truck_{truck_id}'

        subscription = TruckSubscription(
            channel_name,
            request,
            ws
        )
        truck_job = await subscription.start()
    except Exception:
        return ws

    try:
        request.app['websockets'].add(ws)

        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == 'close':
                    await ws.close()
                    break
            if msg.type == aiohttp.WSMsgType.ERROR:
                logging.error(f'ws connection was closed with exception {ws.exception()}')
    finally:
        await truck_job.close()
        request.app['websockets'].discard(ws)

    return ws


@truck_routes.get('/truck-events', name='truck_events')
async def truck_events_view(request: web.Request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    try:
        subscription = EventSubscription('truck_events', request, ws)
        truck_job = await subscription.start()
    except Exception:
        return ws

    try:
        request.app['websockets'].add(ws)

        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == 'close':
                    await ws.close()
                    break
            if msg.type == aiohttp.WSMsgType.ERROR:
                logging.error(f'ws connection was closed with exception {ws.exception()}')
    finally:
        await truck_job.close()
        request.app['websockets'].discard(ws)

    return ws

