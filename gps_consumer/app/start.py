import logging
import weakref
from logging.handlers import RotatingFileHandler

import aioredis
import aiohttp_cors

from typing import AnyStr, Dict
from aiohttp import web, WSCloseCode
from aiojobs.aiohttp import setup

from utils.config import read_config
from gps_consumer.app.views import truck_routes
from gps_consumer.settings import CONFIG_FILE, LOG_FILE
from gps_consumer.subscriptions.channel_producer import ChannelProducer


def create_app():
    init_logging()

    app = web.Application()
    app['config'] = read_config(CONFIG_FILE)
    app.router.add_routes([*truck_routes])

    app.cleanup_ctx.append(init_redis)

    app['websockets'] = weakref.WeakSet()
    app['truck_events_producer']: ChannelProducer = None
    app['truck_state_producers']: Dict[AnyStr, ChannelProducer] = {}

    app.on_shutdown.extend([
        close_producers,
        close_ws_connections
    ])

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    for route in app.router.routes():
        cors.add(route)

    setup(app)

    return app


async def init_redis(app: web.Application):
    config = app['config']['redis']
    redis = await aioredis.create_redis_pool(config['address'])
    app['redis'] = redis

    yield
    app['redis'].close()
    await app['redis'].wait_closed()


async def close_producers(app: web.Application):
    for producer in app['truck_state_producers'].values():
        producer.close()

    producer = app['truck_events_producer']
    if producer:
        producer.close()


async def close_ws_connections(app: web.Application):
    for ws in set(app['websockets']):
        await ws.close(
            code=WSCloseCode.GOING_AWAY,
            message='Server shutdown'
        )


def init_logging(file_name: str = LOG_FILE):
    log_file = RotatingFileHandler(
        filename=file_name,
        maxBytes=1024 * 1024,
        backupCount=3,
        encoding='utf-8'
    )

    log_file.setLevel(logging.INFO)

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s|gps_consumer|%(filename)20s[%(lineno)3s] %(levelname)8s : %(message)s',
        handlers=[log_file, console]
    )

    logging.basicConfig(level=logging.DEBUG)
