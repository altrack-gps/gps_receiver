from aiohttp import web

from gps_consumer.app.start import create_app


def main():
    app = create_app()
    http_config = app['config']['http']

    web.run_app(
        app=app,
        host=http_config['host'],
        port=int(http_config.getint('port', 8080))
    )


if __name__ == '__main__':
    main()
