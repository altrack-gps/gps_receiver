from aiohttp import web
from asynctest import patch, CoroutineMock, Mock
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from gps_consumer.subscriptions import channel_producer
from gps_consumer.app.start import create_app
from gps_consumer.subscriptions import setup_truck_subscription
from redis.connection import Redis
from redis.helpers import save_truck_message

from serializers import TruckStateMessage
from tests.utils import get_test_truck, get_test_truck_message


class AppTestCase(AioHTTPTestCase):
    def setUp(self):
        super().setUp()
        self.truck = get_test_truck()

    async def get_application(self):
        return create_app()


class TestApp(AppTestCase):
    def setUp(self):
        super().setUp()
        self.truck = get_test_truck()
        self.ws_url = self.app.router['truck_gps_state'].url_for(
            id=str(self.truck.id)
        )

        redis = self.loop.run_until_complete(
            Redis.create('redis://localhost:6379')
        )
        Redis.set_current(redis)

    @patch('aiojobs._job.Job')
    @patch('gps_consumer.app.views.setup_truck_subscription')
    @unittest_run_loop
    async def test_ws_connection(
            self,
            setup_mock,
            job_mock
    ):
        job_mock.close = CoroutineMock()
        setup_mock.return_value = job_mock

        async with self.client.ws_connect(self.ws_url):
            ...

        setup_mock.assert_awaited()
        job_mock.close.assert_awaited()

    @unittest_run_loop
    async def test_receiving_message(self):
        gps_message = get_test_truck_message()

        async with self.client.ws_connect(self.ws_url) as ws:
            await save_truck_message(gps_message)
            received_state = await ws.receive()

            self.assertEqual(
                gps_message,
                TruckStateMessage(**received_state.json())
            )


class TestSetupTruckStateListening(AppTestCase):
    @patch('gps_consumer.truck_producer._run_truck_subscription')
    @patch('gps_consumer.truck_producer._init_truck_producer')
    @unittest_run_loop
    async def test_run_truck_subscription(
            self,
            init_producer_mock,
            run_subscription_mock
    ):

        channel_name = f'truck_{self.truck.id}'

        def set_channel():
            self.app['truck_state_producers'][channel_name] = Mock()
        init_producer_mock.side_effect = [set_channel()]

        request = Mock()
        request.match_info = {'id': self.truck.id}
        request.app = self.app
        ws = web.WebSocketResponse()

        await setup_truck_subscription(request, ws)

        init_producer_mock.assert_awaited_with(channel_name, self.app)
        run_subscription_mock.assert_awaited_with(
            request=request,
            ws=ws,
            truck_producer=self.app['truck_state_producers'][channel_name]
        )


class TestInitTruckStateProducer(AppTestCase):
    @unittest_run_loop
    async def test_init(self):
        mock = CoroutineMock()
        mock.return_value = [Mock()]
        self.app['redis'].subscribe = mock

        channel_name = f'truck_{self.truck.id}'
        with self.assertRaises(KeyError):
            producer = self.app['truck_state_producers'][channel_name]

        await channel_producer._init_truck_producer(channel_name, self.app)
        producer = self.app['truck_state_producers'][channel_name]

        mock.assert_awaited_with(channel_name)


class TestRunTruckSubscription(AppTestCase):
    @patch('gps_consumer.truck_producer.spawn')
    @unittest_run_loop
    async def test_run(
            self,
            spawn_mock
    ):
        spawn_mock.return_value = Mock()

        request = Mock()
        ws = web.WebSocketResponse()
        producer = Mock()

        await channel_producer._run_truck_subscription(
            request=request,
            ws=ws,
            truck_producer=producer
        )

        spawn_mock.assert_awaited()
