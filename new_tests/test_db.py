import pytest

from db.models import t_trucks
from utils.db.test_utils import create_tables, drop_tables


@pytest.fixture
def connect(make_sa_connection, loop):
    async def start():
        conn = await make_sa_connection()

        await drop_tables(conn)
        await create_tables(conn)

        await conn.execute(
            t_trucks.insert().values(imei='1111', plate_number='AA3434AA')
        )
        return conn

    return loop.run_until_complete(start())


async def test_insert(connect):
    res = await connect.execute(t_trucks.select())
    print('bube')
    print([
        dict(t) async for t in res
    ])

    await connect.execute(
        t_trucks.insert().values(imei='342322', plate_number='AA3438HA')
    )

    # await connect.execute(tbl.insert().values(id='te/st-4', name='test_name'))
    # await connect.execute(tbl.insert().values(id='test-5', name='test_name'))
    # assert 5 == len(await (await connect.execute(tbl.select())).fetchall())


async def test_get(connect):
    res = await connect.execute(t_trucks.select())
    print([
        dict(t) async for t in res
    ])
