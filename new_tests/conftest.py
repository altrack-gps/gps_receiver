import gc
import time
import uuid
import socket
import pytest
import asyncio
import psycopg2

import aiopg
from aiohttp.pytest_plugin import _passthrough_loop_context

from aiopg import sa
from docker import APIClient

from gps_producer.server import GPSServer
from gps_producer.settings import CONFIG_FILE
from utils.config import read_config


@pytest.fixture(scope='session')
def unused_port():
    def f():
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind(('127.0.0.1', 0))
            return s.getsockname()[1]

    return f


@pytest.fixture
def loop(request):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(None)

    yield loop

    if not loop._closed:
        loop.call_soon(loop.stop)
        loop.run_forever()
        loop.close()
    gc.collect()
    asyncio.set_event_loop(None)


@pytest.fixture(scope='session')
def session_id():
    '''Unique session identifier, random string.'''
    return str(uuid.uuid4())


@pytest.fixture(scope='session')
def docker():
    return APIClient(version='auto')


def pytest_addoption(parser):
    parser.addoption("--pg_tag", action="append", default=[],
                     help=("Postgres server versions. "
                           "May be used several times. "
                           "Available values: 9.3, 9.4, 9.5, all"))
    parser.addoption(
        "--no-pull",
        action="store_true",
        default=False,
        help="Don't perform docker images pulling"
    )


def pytest_generate_tests(metafunc):
    if 'pg_tag' in metafunc.fixturenames:
        tags = set(metafunc.config.option.pg_tag)
        if not tags:
            tags = ['9.5']
        elif 'all' in tags:
            tags = ['9.3', '9.4', '9.5']
        else:
            tags = list(tags)

        metafunc.parametrize("pg_tag", tags, scope='session')


@pytest.fixture(scope='session')
def pg_server(unused_port, docker, session_id, pg_tag, request):
    # if not request.config.option.no_pull:
    #     docker.pull(f'mdillon/postgis:{pg_tag}')

    container_args = dict(
        image=f'mdillon/postgis:{pg_tag}',
        name='aiopg-test-server-{}-{}'.format(pg_tag, session_id),
        ports=[5432],
        detach=True,
    )

    # bound IPs do not work on OSX
    host = "127.0.0.1"
    host_port = unused_port()
    container_args['host_config'] = docker.create_host_config(
        port_bindings={5432: (host, host_port)})

    container = docker.create_container(**container_args)

    try:
        docker.start(container=container['Id'])
        server_params = dict(database='postgres',
                             user='postgres',
                             password='mysecretpassword',
                             host=host,
                             port=host_port)
        delay = 0.001
        for i in range(100):
            try:
                conn = psycopg2.connect(**server_params)
                cur = conn.cursor()
                cur.execute("CREATE EXTENSION hstore;")
                cur.close()
                conn.close()
                break
            except psycopg2.Error:
                time.sleep(delay)
                delay *= 2
        else:
            pytest.fail("Cannot start postgres server")

        container['host'] = host
        container['port'] = host_port
        container['pg_params'] = server_params

        yield container
    finally:
        docker.kill(container=container['Id'])
        docker.remove_container(container['Id'])


@pytest.fixture
def pg_params(pg_server):
    return dict(**pg_server['pg_params'])


@pytest.fixture
def make_connection(loop, pg_params):
    conns = []

    async def go(*, no_loop=False, **kwargs):
        nonlocal conns
        params = pg_params.copy()
        params.update(kwargs)
        useloop = None if no_loop else loop
        conn = await aiopg.connect(loop=useloop, **params)
        conn2 = await aiopg.connect(loop=useloop, **params)
        cur = await conn2.cursor()
        await cur.execute("DROP TABLE IF EXISTS foo")
        await conn2.close()
        conns.append(conn)
        return conn

    yield go

    for conn in conns:
        loop.run_until_complete(conn.close())


@pytest.fixture
def create_pool(loop, pg_params):
    pool = None

    async def go(*, no_loop=False, **kwargs):
        nonlocal pool
        params = pg_params.copy()
        params.update(kwargs)
        useloop = None if no_loop else loop
        pool = await aiopg.create_pool(loop=useloop, **params)
        return pool

    yield go

    if pool is not None:
        pool.terminate()
        loop.run_until_complete(pool.wait_closed())


@pytest.fixture
def make_engine(loop, pg_params):
    engine = engine_use_loop = None

    async def go(*, use_loop=True, **kwargs):
        nonlocal engine, engine_use_loop
        pg_params.update(kwargs)
        if use_loop:
            engine_use_loop = engine_use_loop or (
                await sa.create_engine(loop=loop, **pg_params)
            )
            return engine_use_loop
        else:
            engine = engine or (await sa.create_engine(**pg_params))
            return engine

    yield go

    if engine_use_loop is not None:
        engine_use_loop.close()
        loop.run_until_complete(engine_use_loop.wait_closed())

    if engine is not None:
        engine.close()
        loop.run_until_complete(engine.wait_closed())


@pytest.fixture
def make_sa_connection(make_engine, loop):
    conns = []
    engine = None

    async def go(*, use_loop=False, **kwargs):
        nonlocal conns, engine
        engine = engine or (await make_engine(use_loop=use_loop, **kwargs))
        conn = await engine.acquire()
        conns.append(conn)
        return conn

    yield go

    for conn in conns:
        loop.run_until_complete(conn.close())


@pytest.mark.tryfirst
def pytest_pyfunc_call(pyfuncitem):
    """
    Run asyncio marked test functions in an event loop instead of a normal
    function call.
    """
    if asyncio.iscoroutinefunction(pyfuncitem.function):
        existing_loop = pyfuncitem.funcargs.get('loop', None)
        with _passthrough_loop_context(existing_loop) as _loop:
            testargs = {arg: pyfuncitem.funcargs[arg]
                        for arg in pyfuncitem._fixtureinfo.argnames}

            task = _loop.create_task(pyfuncitem.obj(**testargs))
            _loop.run_until_complete(task)

        return True


@pytest.fixture
def gps_server():
    server = None

    async def go():
        nonlocal server

        config = read_config(CONFIG_FILE)
        config['tcp']['port'] = str(8089)

        server = await GPSServer.create(config)


# @pytest.fixture
# def clear_db_data(make_sa_connection, loop):
#     async def clear():
#         conn = await make_sa_connection()
#         await clear_tables(conn)
#
#     return loop.run_until_complete(clear())
