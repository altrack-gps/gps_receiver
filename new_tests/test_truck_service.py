from asynctest import patch, Mock
from shapely.geometry import Point

from db.models import Route
from gps_producer.services import TruckService
from tests.utils import get_test_truck


@patch('services.truck.get_distance_from_route')
@patch('services.truck.get_route_by_id')
async def test_is_on_route(get_route_mock, distance_mock):
    route = Mock(from_spec=Route)
    get_route_mock.return_value = route
    distance_mock.return_value = 1

    truck = get_test_truck()
    await TruckService.is_on_route(truck, Point([1, 2]))

    get_route_mock.assert_awaited_with(truck.route_id)
    distance_mock.assert_awaited()
