from configparser import ConfigParser


def read_config(file_name):
    config = ConfigParser()
    config.read(file_name)

    return config
