import psycopg2

from aiopg.sa import SAConnection
from sqlalchemy.schema import CreateTable
from sqlalchemy.sql.ddl import DropTable

from db.models import tables, metadata


async def create_tables(conn: SAConnection):
    for table in tables:
        await conn.execute(CreateTable(table))


async def drop_tables(conn: SAConnection):
    try:
        for table in metadata.sorted_tables.reverse():
            await conn.execute(DropTable(table))
    except psycopg2.ProgrammingError:
        pass


async def clear_tables(conn: SAConnection):
    for table in tables:
        await conn.execute(table.delete(CASCADE=True))
