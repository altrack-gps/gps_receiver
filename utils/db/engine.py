from contextlib import asynccontextmanager

from aiopg.sa import create_engine, Engine

from mixins import ContextInstanceMixin


class DBEngine(ContextInstanceMixin):
    _engine: Engine

    @classmethod
    async def create(cls, dsn: str):
        self = cls()
        self._engine = await create_engine(dsn)

        return self

    async def close(self):
        self._engine.close()
        await self._engine.wait_closed()

    @asynccontextmanager
    async def acquire(self):
        async with self._engine.acquire() as conn:
            yield conn
