"""
    Datetime utils
"""

from datetime import datetime


_FORMAT = '%Y-%m-%d %H:%M:%S'


def from_dt_to_str(dt: datetime) -> str:
    return dt.strftime(_FORMAT)


def from_str_to_dt(dt_str: str) -> datetime:
    return datetime.strptime(dt_str, _FORMAT)


def dt_delta(dt1: datetime, dt2: datetime) -> float:
    """
    :return: difference in minutes
    """
    return (dt1 - dt2).total_seconds() / 60
