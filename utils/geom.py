from shapely.geometry import Point


def to_point_from_coords(lat: float, lng: float) -> Point:
    return Point([lat, lng])


def to_point_from_dict(point: dict) -> Point:
    return Point(point.values())
