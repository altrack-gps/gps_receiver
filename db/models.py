from datetime import datetime
from enum import Enum

from shapely.geometry import Point

from geoalchemy2 import Geography
from geoalchemy2.shape import to_shape, from_shape

from sqlalchemy import String, Column, INT, Float, TIMESTAMP, ForeignKey, Integer, DateTime, Enum as DBEnum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import JSON

from serializers import GPSMessage, TruckEventType


Base = declarative_base()
metadata = Base.metadata


class TableList:
    def __init__(self):
        self._tables = []

    def table(self, klass):
        self._tables.append(klass.__table__)
        return klass

    @property
    def tables(self) -> list:
        return self._tables

    def __getitem__(self, index):
        return self._tables[index]


tables = TableList()


@tables.table
class Route(Base):
    __tablename__ = 'routes'

    id = Column(
        Integer,
        primary_key=True
    )
    title = Column(
        String(50),
        unique=True
    )
    duration = Column(DateTime)
    distance = Column(Float)
    line = Column(
        Geography(
            geometry_type='LINESTRING',
            srid=4326
        )
    )


t_routes = Route.__table__


@tables.table
class Truck(Base):
    __tablename__ = 'trucks'

    id = Column(
        Integer,
        primary_key=True
    )
    imei = Column(
        String(20),
        nullable=False,
        unique=True
    )
    plate_number = Column(
        String(8),
        nullable=False,
        unique=True
    )

    route_id = Column(
        ForeignKey('routes.id', ondelete='SET NULL'),
        nullable=True
    )


t_trucks = Truck.__table__


@tables.table
class TruckState(Base):
    __tablename__ = 'truck_states'

    truck_id = Column(
        INT,
        ForeignKey('trucks.id'),
        primary_key=True
    )
    position = Column(
        Geography(geometry_type='POINT', srid=4326),
        nullable=False
    )
    course = Column(
        INT,
        nullable=False
    )
    speed = Column(
        Float,
        nullable=False
    )
    state_at = Column(
        TIMESTAMP,
        nullable=False
    )

    @classmethod
    def from_message(cls, truck_id, message: GPSMessage):
        self = cls()
        self.truck_id = truck_id
        self.state_at = message.state_at
        self.course = message.course
        self.speed = message.speed
        self.position = from_shape(
            shape=Point(message.lat, message.lon),
            srid=4326
        )

        return self

    def __repr__(self):
        return f'<TruckState(truck_id={self.truck_id}, speed={self.speed}, ' \
               f'course={self.course}, state_at={self.state_at}, position={to_shape(self.position)})>'


t_truck_state = TruckState.__table__


class WayPoint(Base):
    __tablename__ = 'waypoints'

    id = Column(INT, primary_key=True)
    position = Column(
        Geography(geometry_type='POINT', srid=4326),
        nullable=False
    )
    number = Column(INT)
    route_id = Column(ForeignKey('routes.id'))
    address_id = Column(ForeignKey('waypoint_addresses.id'))

    def __repr__(self):
        return f'<WayPoint(id={self.id}, number={self.number}, route_id={self.route_id}, address_id={self.address_id})>'


t_way_points = WayPoint.__table__


class LocalityTypes(Enum):
    CITY = 'CITY'
    VILLAGE = 'VILLAGE'


class Address(Base):
    __tablename__ = 'waypoint_addresses'

    id = Column(INT, primary_key=True)
    house_number = Column(String(5), nullable=True)
    road = Column(String(50), nullable=True)
    locality = Column(String(50), nullable=True)
    locality_type = Column(DBEnum(LocalityTypes))
    state = Column(String(50), nullable=True)
    country = Column(String(50), nullable=True)

    def __repr__(self):
        return f'<Address(id={self.id}, house={self.house_number}, road={self.road}, locality={self.locality}, ' \
               f'locality_type={self.locality_type}, state={self.state}, country={self.country})>'

    def to_str(self):
        locality_type = 'г' if self.locality_type is LocalityTypes.CITY else 'с.'
        return f'Буд.{self.house_number}, {self.road}, {locality_type}.{self.locality}, обл.{self.state}'


t_addresses = Address.__table__


class TruckEvent(Base):
    __tablename__ = 'truck_events'

    id = Column(INT, primary_key=True)
    truck_id = Column(ForeignKey('trucks.id'))
    payload = Column(JSON, default={})
    event_at = Column(DateTime)
    type = Column(DBEnum(TruckEventType))

    def __init__(self, *args, **kwargs) -> None:
        if 'event_at' not in kwargs:
            kwargs['event_at'] = datetime.now()
        super().__init__(*args, **kwargs)

    def __repr__(self):
        return f'<TruckEvent(id={self.id}, truck_id={self.truck_id}, ' \
               f'event_at={self.event_at}, type={self.type}, payload={self.payload})>'


t_truck_events = TruckEvent.__table__


class TruckReport(Base):
    __tablename__ = 'truck_reports'

    id = Column(INT, primary_key=True)
    truck_id = Column(ForeignKey('trucks.id'))
    route_id = Column(ForeignKey('routes.id'))
    start_date = Column(DateTime, nullable=True)
    finish_date = Column(DateTime, nullable=True)

    def __repr__(self):
        return f'<{self.__class__.__name__}(id={self.id}, truck_id={self.truck_id}, route_id={self.route_id}, ' \
               f'start_date={self.start_date}, finish_date={self.finish_date})>'


t_truck_reports = TruckReport.__table__
