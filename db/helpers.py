from datetime import datetime

from sqlalchemy import select, func, exists, and_
from geoalchemy2.shape import from_shape
from sqlalchemy.sql import Select

from utils.db.engine import DBEngine
from exceptions import TruckDoesNotExist
from db.models import TruckState, t_truck_state, Truck, t_trucks, t_routes, Route, t_addresses, t_way_points, WayPoint, \
    Address, TruckEvent, t_truck_events, t_truck_reports, TruckReport


async def save_truck_state(
        state: TruckState
):
    query = t_truck_state.insert().values(
        truck_id=state.truck_id,
        state_at=state.state_at,
        position=state.position,
        course=state.course,
        speed=state.speed
    )
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        await conn.execute(query)


async def get_all_truck_states():
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        cursor = await conn.execute(t_truck_state.select())
        return [
            TruckState(**row)
            async for row in cursor
        ]


async def get_truck_by_imei(imei: str) -> Truck:
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        cursor = await conn.execute(
            t_trucks.select().where(Truck.imei == imei)
        )
        truck = await cursor.fetchone()
        cursor.close()

        if not truck:
            raise TruckDoesNotExist

        return Truck(**dict(truck))


async def update_truck_route(truck_id: int, route_id: int):
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        await conn.execute(
            t_trucks.update().values(route_id=route_id).where(Truck.id == truck_id)
        )


async def get_route_by_id(route_id: int) -> Route:
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        cursor = await conn.execute(
            t_routes.select().where(Route.id == route_id)
        )
        route = await cursor.fetchone()
        cursor.close()

        return Route(**dict(route))


async def get_distance_from_route(line, point) -> float:
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        query = select([
            func.ST_Distance(
                from_shape(
                    shape=point,
                    srid=4326
                ),
                line
            )
        ])

        distance = await conn.scalar(query) * 100  # converting into km
        return distance


async def has_truck_route(imei: str) -> bool:
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        return await conn.scalar(
            select([
                exists().where(
                    and_(
                        Truck.imei == imei,
                        Truck.route_id != None
                    )
                )
            ])
        )


async def get_closest_wp(truck_id, point, max_distance=300) -> WayPoint:
    select_from = t_way_points.join(
        t_routes.join(t_trucks)
    )
    distance = func.ST_Distance(
        from_shape(
            shape=point,
            srid=4326
        ),
        WayPoint.position
    )
    query = select([t_way_points]).select_from(select_from).where(
        and_(
            Truck.id == truck_id,
            distance < max_distance
        )
    ).order_by(distance)

    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        result = await conn.execute(query)
        way_point = await result.fetchone()
        result.close()

        return WayPoint(**dict(way_point)) if way_point else None


async def get_last_wp(route_id: int):
    max_number = select([func.max(WayPoint.number)])\
        .select_from(WayPoint)\
        .where(WayPoint.route_id == route_id)

    query = t_way_points.select()\
        .where(
            and_(
                WayPoint.route_id == route_id,
                WayPoint.number == max_number
            )
        )
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        result = await conn.execute(query)
        wp = await result.fetchone()
        return WayPoint(**dict(wp)) if wp else None


async def get_address_by_wp_id(wp_id: int):
    query = select([t_addresses])\
        .select_from(t_way_points.join(t_addresses))\
        .where(WayPoint.id == wp_id)

    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        cursor = await conn.execute(query)
        address = await cursor.fetchone()
        cursor.close()

        return Address(**dict(address)) if address else None


async def save_truck_event(event: TruckEvent):
    query = t_truck_events.insert()\
        .values(
            type=event.type,
            payload=event.payload,
            event_at=event.event_at,
            truck_id=event.truck_id
        )

    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        await conn.execute(query)


async def get_truck_events():
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        cursor = await conn.execute(
            t_truck_events.select()
        )
        return [
            TruckEvent(**row)
            async for row in cursor
        ]


async def create_report(truck_id: int, route_id: int):
    query = t_truck_reports.insert().values(
        truck_id=truck_id,
        route_id=route_id,
        start_date=datetime.now()
    )

    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        await conn.execute(query)


async def finish_report(truck_id: int):
    current_report = await get_current_report(truck_id)

    query = t_truck_reports.update().values(
        finish_date=datetime.now()
    ).where(
        TruckReport.id == current_report.id
    )
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        await conn.execute(query)


async def get_current_report(truck_id: int):
    query = current_report_query(truck_id)
    engine = DBEngine.get_current()

    async with engine.acquire() as conn:
        result = await conn.execute(query)
        report = await result.fetchone()
        result.close()

        return TruckReport(**dict(report)) if report else None


def current_report_query(truck_id: int) -> Select:
    report_id = select([func.max(TruckReport.id)])\
        .select_from(TruckReport)\
        .where(TruckReport.truck_id == truck_id)

    return t_truck_reports.select().where(
        and_(
            TruckReport.finish_date == None,
            TruckReport.id == report_id
        )
    )


async def current_report_exists(truck_id: int) -> bool:
    query = select([
        exists(
            current_report_query(truck_id)
        )
    ])
    engine = DBEngine.get_current()
    async with engine.acquire() as conn:
        result = await conn.execute(query)
        return await result.scalar()
