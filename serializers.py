import re
import json

from enum import Enum
from datetime import datetime

from pydantic import BaseModel, validator


class GPSData(BaseModel):
    lat: float
    lon: float
    speed: int
    course: int
    state_at: datetime

    def json_dict(self):
        return json.loads(self.json())


class GPSMessage(GPSData):
    imei: str

    @validator('imei')
    def only_numbers(cls, value):
        if not re.match('^\d+$', value):
            raise ValueError('Imei must contain only numbers')
        return value


class RoutePresenceLog(BaseModel):
    truck_id: int
    has_route: int = 0
    is_on_route: int = 0
    had_route: int = 0
    was_on_route: int = 0


class TruckEventType(Enum):
    LEFT_ROUTE = 'LEFT_ROUTE'
    START_ROUTE = 'START_ROUTE'
    FINISH_ROUTE = 'FINISH_ROUTE'
    MOVE_ON_ROUTE = 'MOVE_ON_ROUTE'
    PASSING_WAY_POINT = 'PASSING_WAY_POINT'


class TruckEventMsg(BaseModel):
    imei: str
    plate_number: str
    event_at: datetime
    type: TruckEventType
    payload: dict = None

    @classmethod
    def from_truck_event(cls, truck, event):
        return cls(
            imei=truck.imei,
            plate_number=truck.plate_number,
            type=event.type,
            payload=event.payload,
            event_at=event.event_at
        )
